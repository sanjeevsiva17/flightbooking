# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from . import models
# Register your models here.
class ticketAdmin(admin.ModelAdmin):
    list_display = ("From", "To","Flightname","number")
    # prepopulated_fields = ("Flightname")

admin.site.register(models.ticket,ticketAdmin)
